from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from django.contrib.admin.utils import unquote

from .models import Kelurahan


class CustomAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name", "kecamatan__name")

    class Meta:
        model = 'Kelurahan'

admin.site.register(Kelurahan, CustomAdmin)
