import random
import string
from datetime import datetime, timedelta

from django.apps.registry import apps
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        UserManager)
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from kartanu.core.utils import FilenameGenerator
from kartanu.core.middleware import get_kecamatan_id, check_is_superuser, is_login
from kartanu.apps.kabupaten.models import Kabupaten
from kartanu.apps.provinsi.models import Provinsi

from model_utils import Choices
from sequences import get_next_value


class CustomUserManager(UserManager):
    def create_user(self, username, password, **extra_fields):
        now = timezone.now()
        user = self.model(
            username=username,
            is_active=True, is_superuser=False,
            last_login=now, date_joined=now, **extra_fields)

        if password:
            user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password, **extra_fields):
        user = self.create_user(username, password, **extra_fields)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=50, unique=True, db_index=True)
    name = models.CharField(max_length=50, null=True)
    mother_name = models.CharField(max_length=50, null=True)
    nik = models.CharField(max_length=50, unique=True, null=True)
    email = models.EmailField(null=True, blank=True, max_length=50, db_index=True, default=None)
    is_active = models.BooleanField('active', default=True)
    is_staff = models.BooleanField('Status Staff', default=False)
    provinsi = models.ForeignKey(
        'provinsi.Provinsi', on_delete=models.PROTECT, blank=True, null=True)
    kabupaten = models.ForeignKey(
        'kabupaten.Kabupaten', on_delete=models.PROTECT, blank=True, null=True)
    kecamatan = models.ForeignKey(
        'kecamatan.Kecamatan', on_delete=models.PROTECT, null=True)
    kelurahan = models.ForeignKey(
        'kelurahan.Kelurahan', on_delete=models.PROTECT, null=True)
    rt = models.CharField(max_length=50, blank=True, null=True)
    rw = models.CharField(max_length=50, blank=True, null=True)
    mobile_number = models.CharField(max_length=30, default='', blank=True)
    picture = models.ImageField(upload_to=FilenameGenerator(prefix='user'), default='', blank=True)
    ktp = models.ImageField(upload_to=FilenameGenerator(prefix='user'), default='', blank=True)
    place_of_birth = models.CharField(max_length=50, unique=True, null=True)
    from_web = models.BooleanField(default=False)
    dob = models.DateField(blank=True, null=True)
    GENDER = Choices(("L", "Laki Laki"), ("P", "Perempuan"))
    gender = models.CharField(
        max_length=10, choices=GENDER)
    MARITAL_STATUS = Choices(("Kawin", "Kawin"), ("Lajang", "Lajang"))
    marital_status = models.CharField(
        max_length=10, choices=MARITAL_STATUS, default="Lajang")
    job = models.CharField(max_length=20, blank=True, null=True)
    EDUCATION = Choices(
        ("SD", "SD"),
        ("SMP", "SMP"),
        ("SLTP", "SLTP"),
        ("SMA", "SMA"),
        ("SLTA", "SLTA"),
        ("D3", "D3"),
        ("S1", "S1"),
        ("S2", "S2"),
        ("S3", "S3"),
        ("Lainnya", "Lainnya")
    )
    education = models.CharField(max_length=255, blank=True, null=True, choices=EDUCATION)
    id_card_number = models.CharField(max_length=25, blank=True, null=True)
    order = models.IntegerField(default=0, blank=True)
    print_counter = models.IntegerField(default=0)

    USERNAME_FIELD = 'username'
    objects = CustomUserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return '%s - %s' % (self.username, self.name)

    def __unicode__(self):
        return self.name or self.username or self.email or 'User #%d' % (self.id)

    def save(self, *args, **kwargs):
        exist_id = self.id
        super(User, self).save(*args, **kwargs)
        self.kabupaten = Kabupaten.objects.first()
        self.provinsi = Provinsi.objects.first()
        if not exist_id:
            self.order = get_next_value("order_user")
            last_counter = get_next_value(self.kelurahan.code) + 1
            self.id_card_number = f"{self.kelurahan.code}-{str(last_counter).zfill(5)}"
            PrintCard.objects.create(user=self, order=self.order)
            self.save()


class PrintCard(models.Model):
    user = models.ForeignKey(
        'user.User', on_delete=models.CASCADE, unique=True)
    order = models.IntegerField(default=0)

