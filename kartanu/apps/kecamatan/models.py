from django.db import models


class Kecamatan(models.Model):
    name = models.CharField(max_length=254)
    code = models.IntegerField(null=True, default=0)
    kabupaten = models.ForeignKey('kabupaten.Kabupaten', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
