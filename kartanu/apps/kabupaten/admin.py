from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Kabupaten


class KabupatenKotaAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    search_fields = ('name',)
    # autocomplete_fields = ('provinsi')

admin.site.register(Kabupaten, KabupatenKotaAdmin)