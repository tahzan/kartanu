from dal import autocomplete
from django.db.models import Q
from kartanu.apps.user.models import User
from kartanu.apps.kecamatan.models import Kecamatan
from kartanu.apps.kelurahan.models import Kelurahan
from kartanu.apps.kabupaten.models import Kabupaten
from kartanu.apps.provinsi.models import Provinsi

from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class ProvinsiAutoComplete(autocomplete.Select2QuerySetView):
    paginate_by = 36

    def get_queryset(self):
        if self.q:
            qs_provinsi = Provinsi.objects.filter(
                Q(name__icontains=self.q)
            ).distinct()[:5]
        else:
            qs_provinsi = Provinsi.objects.all()[:3]

        return qs_provinsi


class KabupatenAutocomplete(autocomplete.Select2QuerySetView):
    paginate_by = 10

    def get_queryset(self):
        provinsi = self.forwarded.get('provinsi', None)
        if provinsi and self.q:
            qs_kabupaten = Kabupaten.objects.filter(name=provinsi).filter(
                Q(name__icontains=self.q)
            ).distinct()[:5]
        elif provinsi:
            qs_kabupaten = Kabupaten.objects.filter(provinsi=provinsi)[:3]
        else:
            qs_kabupaten = Kabupaten.objects.all()[:3]

        return qs_kabupaten


class KecamatanAutocomplete(autocomplete.Select2QuerySetView):
    paginate_by = 10

    def get_queryset(self):
        kabupaten = self.forwarded.get('kabupaten', None)
        print(self.q)
        if kabupaten and self.q:
            qs_kecamatan = Kecamatan.objects.filter(kabupaten=kabupaten).filter(
                Q(name__icontains=self.q)
            ).distinct()[:5]
        elif kabupaten:
            qs_kecamatan = Kecamatan.objects.filter(kabupaten=kabupaten)[:3]
        else:
            qs_kecamatan = Kecamatan.objects.filter(name__icontains=self.q)[:3]

        return qs_kecamatan


class KelurahanAutocomplete(autocomplete.Select2QuerySetView):
    paginate_by = 10

    def get_queryset(self):
        kecamatan = self.forwarded.get('kecamatan', None)
        if kecamatan and self.q:
            qs_kelurahan = Kelurahan.objects.filter(kecamatan=kecamatan).filter(
                Q(name__icontains=self.q)
            ).distinct()[:5]
        elif kecamatan:
            qs_kelurahan = Kelurahan.objects.filter(kecamatan=kecamatan)[:3]
        else:
            qs_kelurahan = Kelurahan.objects.filter(name=self.q)[:3]

        return qs_kelurahan
