import csv, io, os
from django.core.files import File
from django.apps.registry import apps
from django.contrib import messages
from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from kartanu.apps.user.models import User, PrintCard
from kartanu.apps.kecamatan.models import Kecamatan
from kartanu.apps.kelurahan.models import Kelurahan
from kartanu.apps.user.decorators import login_validate
from kartanu.backoffice.forms import LoginForm, UserCreateForm
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.urls import reverse
from django.db import transaction
from django.http import JsonResponse
from datetime import date


get_model = apps.get_model

def login_view(request):
    auth_form = LoginForm(data=request.POST or None)
    if auth_form.is_valid():
        login(request, auth_form.get_user())
        auth_form.get_user()
        return redirect("backoffice:dashboard")
    else:
        messages.error(request, "Username and Password are incorrect")

    list(messages.get_messages(request))
    context = dict(auth_form=auth_form,)
    return TemplateResponse(request, "backoffice/login.html", context)


def log_out(request):
    logout(request)
    return redirect("backoffice:login")


@login_validate
def dashboard(request):
    users = User.objects.all().exclude(is_superuser=True).exclude(is_staff=True)
    context = dict(
        to_print = users.filter(print_counter=0).count(),
        users_count = users.count(),
        already_printed= users.filter(print_counter__gt=0).count(),
    )
    return TemplateResponse(request, "backoffice/index.html", context)

@login_validate
def users(request):
    users = User.objects.all().order_by("id").exclude(is_superuser=True).exclude(is_staff=True)

    if not request.user.is_superuser:
        users = users.filter(kecamatan=request.user.kecamatan)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    counter = request.GET.get("counter", "")
    if counter:
        users = users.filter(print_counter=int(counter))

    results_per_page = 30
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Users",
        "filter": {"search": search, "counter": counter},
    }
    return TemplateResponse(request, "backoffice/users.html", context)


@login_validate
def user_preview(request, id):
    user = User.objects.all().get(id=id)
    context = {
        "user": user,
    }
    return TemplateResponse(request, "backoffice/user-detail.html", context)


def add_to_print(request, id):
    user = User.objects.all().get(id=id)
    PrintCard.objects.update_or_create(user=user, order=user.order)
    return JsonResponse({"status": "OK"}, safe=False)


@login_validate
def add_user(request):
    kecamatan=request.user.kecamatan
    form = UserCreateForm(request.POST or None, request.FILES or None, 
        initial={'kecamatan': kecamatan})
    
    if form.is_valid():
        form.save()
        messages.success(request, 'success')
        return redirect(reverse('backoffice:users'))
    else:
        print(form.errors)

    context = dict(
        edit=False,
        form=form,
        title='Add User'
    )
    return TemplateResponse(request, 'backoffice/add-user.html', context)



@login_validate
@transaction.atomic
def import_user(request):
    context = {
        'order': 'Order of the CSV should be no, name, nik, gender, profile picture, ktp picture, place of birth, date of birth, rt, rw, desa, kecamatan, marital status, formal education, job, mobile_number, email'
    }

    if request.method == "POST":
        folder_name = request.POST.get('folder-name')
        csv_file = request.FILES.get('csv')
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string)

        with transaction.atomic():
            line = 0
            try:
                for column in csv.reader(io_string, delimiter=';', quotechar="|"):
                    if not column[1]:
                        continue

                    line = line + 1
                
                    kecamatan = Kecamatan.objects.filter(name=column[11].strip()).first()
                    if not kecamatan:
                        raise Exception(f'{column[11]} Kecamatan not found')

                    kelurahan = Kelurahan.objects.filter(name=column[10].strip()).first()
                    if not kelurahan:
                        raise Exception(f'{column[10]} Kelurahan not found')

                    ktp = f'./to_upload/{folder_name}/{column[5]}.jpeg'
                    if not os.path.isfile(ktp):
                        raise Exception(f'KTP picture not found')

                    picture = f'./to_upload/{folder_name}/{column[4]}.jpeg'
                    if not os.path.isfile(picture):
                        raise Exception(f'Profile picture not found')

                    nik = column[2].replace("'", "").replace("`", "")
                    
                    user, created = User.objects.update_or_create(
                        nik=nik,
                        defaults={
                            'username': nik, 
                            'name': column[1], 
                            'gender': column[3], 
                            # profile 4
                            # ktp 5
                            'place_of_birth': column[6], 
                            'dob': column[7], 
                            'rt': column[8],
                            'rw': column[9],
                            'kelurahan': kelurahan, #10
                            'kecamatan': kecamatan, #11
                            'marital_status': column[12],
                            'education': column[13],
                            'job': column[14],
                            'mobile_number': column[15],
                            'email': column[16],
                            # 'mother_name': column[11],
                        }
                    )

                    ktp = open(ktp, 'rb')
                    ktp = File(ktp)
                    user.ktp.save(column[9], ktp)
                    ktp.close()

                    picture = open(picture, 'rb')
                    picture = File(picture)
                    user.picture.save(column[10], picture)
                    picture.close()


                context['success'] = "Data Berhasil Di Import"
            except IndexError as e:
                context['error'] = f"{e}. Line: {line}" 
                transaction.set_rollback(True)
            except Exception as e:
                context['error'] = f"{e}. Line: {line}"
                transaction.set_rollback(True)

        return TemplateResponse(request, 'backoffice/import.html', context)

    return TemplateResponse(request, 'backoffice/import.html', context)



@login_validate
def print_view(request):
    users = PrintCard.objects.all().select_related('user').order_by('order')
    page = request.GET.get("page")
    # search = request.GET.get("search", "")
    # if search:
    #     serials = serials.filter(serial_number=search)

    # status = request.GET.get("status")
    # if status:
    #     serials = serials.filter(status=status)

    # type = request.GET.get("type")
    # if type:
    #     serials = serials.filter(type=type)

    # action = request.GET.get("action")
    # if action == 'export':
    #     response = HttpResponse(
    #         content_type='text/csv',
    #         # headers={'Content-Disposition': 'attachment; filename="qrcode.csv"'},
    #     )
    #     writer = csv.writer(response)
    #     writer.writerow(['No', 'Qr-Code', 'Status'])
    #     idx = 0
    #     for sr in serials:
    #         idx += 1
    #         writer.writerow([idx, sr.serial_number, sr.status])
    #     return response
    results_per_page = 30
    # new_count = serials.filter(status="new").count()
    # redeem_count = serials.filter(status="redeem").count()

    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Print Card",
        # "filter": {"search": search, "status": status, "type": type},
    }

    return TemplateResponse(request, "backoffice/print-view.html", context)


@login_validate
def print_card(request):
    users = PrintCard.objects.all().select_related('user').order_by('order')
    start = request.GET.get("start", 0)
    end = request.GET.get("end", 0)
    users = users.filter(order__gte=start).filter(order__lte=end)


    context = {
        "users": users,
        "title": "Print Card",
        "date": date.today()
    }
    return TemplateResponse(request, 'backoffice/print-card.html', context)


@login_validate
def mark_as_print(request):
    prints = PrintCard.objects.all().select_related('user').order_by('order')
    if (request.POST):
        start = request.POST.get("start", 0)
        end = request.POST.get("end", 0)
        prints = prints.filter(order__gte=start).filter(order__lte=end)
        for prin in prints:
            prin.user.print_counter = prin.user.print_counter + 1
            prin.user.save()
        prints.delete()
        return redirect(reverse('backoffice:print_view'))

    # context = {
    #     "users": prints,
    #     "title": "Print Card",
    # }
    # return redirect(reverse('backoffice:users'))
