from django import forms
from django.contrib import auth
from django.forms.forms import BaseForm
from dal import autocomplete
from kartanu.apps.user.models import User


class BaseUserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    mother_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    nik = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    email = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    mobile_number = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    gender = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'form-control'}), choices=[("P", "Pria"), ("W", "Wanita")])
    education = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'form-control'}), required=False, choices=[
            ("SD", "SD"),
            ("SMP", "SMP"),
            ("SMA", "SMA"),
            ("D3", "D3"),
            ("S1", "S1"),
            ("S2", "S2"),
            ("S3", "S3"),
            ("Lainnya", "Lainnya")
        ])
    ktp = forms.ImageField(required=True)
    picture = forms.ImageField(required=True)
    
    class Meta:
        model = User
        fields = (
            'name',
            'nik',
            'mobile_number',
            'gender',
            'email',
            'kabupaten',
            'kecamatan',
            'kelurahan',
            'rt',
            'rw',
            'ktp',
            'picture',
            'mother_name',
            'education'
        )
        widgets = {
            'kecamatan': autocomplete.ModelSelect2(url='kecamatan-autocomplete', forward=('kabuapaten',), attrs={'data-placeholder': 'Pilih Kecamatan', 'class': 'form-control', 'required': True},),
            'kelurahan': autocomplete.ModelSelect2(url='kelurahan-autocomplete', forward=('kecamatan',), attrs={'data-placeholder': 'Pilih Kelurahan', 'class': 'form-control', 'required': True}),
        }
        # initial_fields = ['kecmatan']

    def __init__(self, *args, **kwargs):
        super(BaseUserForm, self).__init__(*args, **kwargs)


class UserCreateForm(BaseUserForm):

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['commit'] = False
        user = super(UserCreateForm, self).save(*args, **kwargs)
        user.username = user.nik
        user.save()
        return user


# class UserEditForm(BaseUserForm):

#     def __init__(self, *args, **kwargs):
#         super(UserEditForm, self).__init__(*args, **kwargs)
#         self.user = kwargs['instance']

#     def save(self, *args, **kwargs):
#         self.user.save()
#         return self.user


class LoginForm(auth.forms.AuthenticationForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(max_length=30)

    def clean(self):
        return super(LoginForm, self).clean()
