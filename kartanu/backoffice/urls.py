from django.urls import path, include
from .views import (
    login_view, log_out, users, add_user, 
    import_user, print_card, print_view,
    mark_as_print, user_preview, add_to_print,
    dashboard
)

app_name = "backoffice"
urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('user', users, name='users'),
    path('<int:id>/preview/', user_preview, name='user_preview'),
    path('<int:id>/add-to-print/', add_to_print, name='add_to_print'),
    path('login/', login_view, name='login'),
    path('log-out/', log_out, name='log_out'),
    path('add-user/', add_user, name='add_user'),
    path('import-user/', import_user, name='import_user'),
    path('print-view/', print_view, name='print_view'),
    path('print-card/', print_card, name='print_card'),
    path('mark-as-print/', mark_as_print, name='mark_as_print'),
]
