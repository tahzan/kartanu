import csv, io, os
from kartanu.apps.kecamatan.models import Kecamatan
from kartanu.apps.kelurahan.models import Kelurahan

with open('wilayah_brebes_xlsx.csv', 'r', newline='') as csvfile:
    _reader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for column in _reader:
        kelurahan = Kelurahan.objects.filter(name=column[7]).first()
        if not kelurahan:
            print(column[7], "not found")
        else:
            _code = int(column[6])
            if _code:
                kelurahan.code = _code
                kelurahan.save()
        kecamatan = Kecamatan.objects.filter(name=column[5]).first()
        if not kecamatan:
            print("not found")
        else:
            kecamatan.code = column[4]
            kecamatan.save()

